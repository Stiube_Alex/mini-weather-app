//#region Nav Menu
function homePage() {
    document.getElementById('home').style.display = "block";
    document.getElementById('about').style.display = "none";
    document.querySelector('.homePage').style.color = "#ff3232";
    document.querySelector('.aboutPage').style.color = "#323232";
    document.querySelector('.homePage').style.fontWeight = "700";
    document.querySelector('.aboutPage').style.fontWeight = "500";
}

function aboutPage() {
    document.getElementById('about').style.display = "block";
    document.getElementById('home').style.display = "none";
    document.querySelector('.aboutPage').style.color = "#ff3232";
    document.querySelector('.homePage').style.color = "#323232";
    document.querySelector('.homePage').style.fontWeight = "500";
    document.querySelector('.aboutPage').style.fontWeight = "700";
}
//#endregion

//#region  Weather API
var button = document.querySelector('.button');
var inputValue = document.querySelector('.inputValue');
var city = document.querySelector('.name');
var desc = document.querySelector('.desc');
var temp = document.querySelector('.temp');

document.querySelector('.inputValue').addEventListener('keyup', event => {
    if (event.keyCode === 13) {
        document.querySelector('.button').click();
    }
})
var nameValue;
var tempValue;
var descValue;

window.onload = () => {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=bucharest&appid=b01bc47fe9c8e07bd6cbcf99480f6fae`)
        .then(response => response.json())
        .then(data => {
            nameValue = data.name;
            tempValue = data.main.temp;
            descValue = data.weather[0].description;
            tempValue -= 273.15;
            city.innerHTML = nameValue;
            desc.innerHTML = descValue;
            temp.innerHTML = tempValue.toFixed(1) + '&#176;C';
        })
}

button.addEventListener('click', () => {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${inputValue.value}&appid=b01bc47fe9c8e07bd6cbcf99480f6fae`)
        .then(response => response.json())
        .then(data => {
            nameValue = data.name;
            tempValue = data.main.temp;
            descValue = data.weather[0].description;
            tempValue -= 273.15;
            city.innerHTML = nameValue;
            desc.innerHTML = descValue;
            temp.innerHTML = tempValue.toFixed(1) + '&#176;C';
        })
        .catch(err => alert(`Wrong Input`))
})
button.addEventListener('click', () => {
    document.getElementById('C').style.color = '#ff3232',
        document.getElementById('F').style.color = '#323232'
})

var inputClear = document.querySelector('.inputValue');
button.addEventListener('click', () => {
    inputClear.value = "";
    inputClear.blur();
})
//#endregion

function toFahrenheit() {
    if (typeof (tempValue) !== 'undefined') {
        document.querySelector('.temp').innerHTML = (tempValue * 1.8 + 32).toFixed(1) + '&#176;F';
        document.getElementById('C').style.color = '#323232';
        document.getElementById('F').style.color = '#ff3232';
    }
}
function toCelsius() {
    if (typeof (tempValue) !== 'undefined') {
        document.querySelector('.temp').innerHTML = tempValue.toFixed(1) + '&#176;C';
        document.getElementById('C').style.color = '#ff3232';
        document.getElementById('F').style.color = '#323232';
    }
}

////////////////////////////////////////////////////////////////////////
// console.log(navigator.geolocation.getCurrentPosition());



// // Step 1: Get user coordinates

// function getCoordinates() {
//     // var options = {
//     //     enableHighAccuracy: true,
//     //     timeout: 5000,
//     //     maximumAge: 0
//     // };

//     function success(pos) {
//         var lat = pos.coords.latitude.toString();
//         var lng = pos.coords.longitude.toString();
//         // var coordinates = [lat, lng];
//         console.log(`Latitude: ${lat}, Longitude: ${lng}`);
//         // getCity(coordinates);
//         // console.log(coordinates);
//         // return;

//     }

//     // function error(err) {
//     //     console.warn(`ERROR(${err.code}): ${err.message}`);
//     // }
//     if ('geolocation' in navigator) {
//         navigator.geolocation.getCurrentPosition(success/*,error, options */);
//     }

// }
// getCoordinates();


// console.log(navigator);
// navigator.geolocation.getCurrentPosition();